<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\User;
use App\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/article")
 */
class ArticleController extends Controller
{
    /**
     * @Route("/", name="article_index", methods="GET")
     */
    public function index(): Response
    {
        $user = $this->getUser()->getUsername();
        $articles = $this->getDoctrine()
            ->getRepository(Article::class)
            ->findAll();

        return $this->render('article/index.html.twig', [
            'articles' => $articles,
            'user' => $user
        ]);
    }

    /**
     * @Route("/new", name="article_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $article = new Article();
        $user = $this->getUser()->getUsername();
        $article->setAuthor($user);

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', "Article was successfully created!");
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="article_show", methods="GET")
     */
    public function show(Article $article): Response
    {
        return $this->render('article/show.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/{id}/edit", name="article_edit", methods="GET|POST")
     */
    public function edit(Request $request, Article $article): Response
    {
        $author = $article->getAuthor();
        $user = $this->getUser()->getUsername();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);


        if($author === $user || $this->getUser()->getRole() === 'ROLE_ADMIN')
        {


            if ($form->isSubmitted() && $form->isValid()) {
                $this->addFlash('success', "Article was successfully updated!");
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('article_edit', ['id' => $article->getId()]);
            }

            return $this->render('article/edit.html.twig', [
                'article' => $article,
                'form' => $form->createView(),
            ]);
        } else{
            return new Response('Permission denied!!! Go AWAY!!!');
        }


    }

    /**
     * @Route("/{id}", name="article_delete", methods="DELETE")
     */
    public function delete(Request $request, Article $article): Response
    {
        $author = $article->getAuthor();
        $user = $this->getUser()->getUsername();
        if($author === $user || $this->getUser()->getRole() === 'ROLE_ADMIN'){



            if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
                $this->addFlash('success', "Article was successfully deleted!");
                $em = $this->getDoctrine()->getManager();
                $em->remove($article);
                $em->flush();
            }

            return $this->redirectToRoute('article_index');
        } else {
            return new Response('Permission denied!!! Go AWAY!!!');
        }


    }
}
