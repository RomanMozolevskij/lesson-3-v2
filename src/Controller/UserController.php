<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="user_index", methods="GET")
     */
    public function index(): Response
    {
        $username = $this->getUser()->getUsername();
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->render('user/index.html.twig', [
            'users' => $users,
            'username' => $username
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods="GET|POST")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($this->getUser()->getRole() === 'ROLE_ADMIN')
        {
            if($form->isSubmitted() && $form->isValid()){
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();


                return $this->redirectToRoute('user_index');
            }
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods="GET")
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', ['user' => $user]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods="GET|POST")
     */
    public function edit(Request $request, User $user): Response
    {
        $username = $user->getUsername();
        $currentUser = $this->getUser()->getUsername();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);


        if($username === $currentUser || $this->getUser()->getRole() === 'ROLE_ADMIN')
        {
            if ($form->isSubmitted() && $form->isValid()) {
                $this->addFlash('success', "User was successfully updated!");
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
            }

            return $this->render('user/edit.html.twig', [
                'user' => $user,
                'form' => $form->createView(),
            ]);
        } else {
            return new Response("Permission denied!!! You crazy?!?!?");
        }
    }

    /**
     * @Route("/{id}", name="user_delete", methods="DELETE")
     */
    public function delete(Request $request, User $user): Response
    {
        $username = $user->getUsername();
        $currentUser = $this->getUser()->getUsername();

        if($username === $currentUser || $this->getUser()->getRole() === 'ROLE_ADMIN'){

            if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
                $this->addFlash('success', "User was successfully deleted!");
                $em = $this->getDoctrine()->getManager();
                $em->remove($user);
                $em->flush();
            }

            return $this->redirectToRoute('user_index');
        } else {
            return new Response("Permission denied!!! You crazy?!?!?");
        }
    }
}
