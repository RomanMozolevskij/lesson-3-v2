<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 29/04/2018
 * Time: 15:53
 */

namespace App\Controller;



use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
        public function showAction(): Response
        {
            $articles = $this->getDoctrine()
                ->getRepository(Article::class)
                ->findAll();
            return $this->render('main/main.html.twig',
                [
                    'articles' => $articles
                ]);
         }
}