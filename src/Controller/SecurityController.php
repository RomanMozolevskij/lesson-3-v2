<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 29/04/2018
 * Time: 15:45
 */

namespace App\Controller;




use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        return $this->render('security/login.html.twig');
    }

    public function logoutAction()
    {

    }
}